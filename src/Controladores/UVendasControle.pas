unit UVendasControle;

interface

uses
  UControle, FireDAC.Comp.Client, FIREDAC.DApt;

type
  TVendasControle = class(TControle)
  public
    function CarregarGrid(const NomeTabela, Filter: String; Sort: String = ''): TFDquery;
    function ChecarProdutosEspeciais(qry : TFDQuery) : Boolean;
  end;

var
  VendasControle : TVendasControle;

implementation

uses
  DB, UConexaoControle, SysUtils;

{ TVendasControle }

function TVendasControle.CarregarGrid(const NomeTabela, Filter: String;
  Sort: String): TFDquery;
var
  qry: TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   qry.SQL.Add(' SELECT V.CODIGO,                                                                        ');
   qry.SQL.Add('        V.CODIGO_CLIENTE,                                                                ');
   qry.SQL.Add('        V.CODIGO_TECNICO,                                                                ');
   qry.SQL.Add('        C.NOME AS NOME_CLIENTE,                                                          ');
   qry.SQL.Add('        V.DATA,                                                                          ');
   qry.SQL.Add('        CASE V.STATUS WHEN ''C'' THEN ''Conclu�do''                                      ');
   qry.SQL.Add('                      WHEN ''A'' THEN ''Aguardando Receita''                             ');
   qry.SQL.Add('                      ELSE NULL                                                          ');
   qry.SQL.Add('        END AS STATUS,                                                                   ');
   qry.SQL.Add('        V.TOTAL_VENDA                                                                    ');
   qry.SQL.Add(Format(' FROM %s V INNER JOIN TBLCLIENTE C ON V.CODIGO_CLIENTE = C.CODIGO ', [NomeTabela]) );

   if not (Trim(Filter) = '') then
     qry.SQL.Add(Format(' WHERE %s ', [Filter]));

   if not (Trim(Sort) = '') then
    qry.SQL.Add(Format(' ORDER BY %s ', [Sort]));

   qry.Open;

   Result := qry;
  finally
  end;
end;

function TVendasControle.ChecarProdutosEspeciais(qry: TFDQuery): Boolean;
begin
  qry.Filter   := ' CONTROLE = ''S'' ';
  qry.Filtered := True;

  Result := not qry.IsEmpty;

  qry.Filtered := False;
end;

end.
