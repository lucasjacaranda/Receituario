unit UControle;

interface

uses
  FireDAC.Comp.Client, FIREDAC.DApt;

  type
    TControle = class
    public
      function CarregarGrid(const NomeTabela, Filter: String; Sort: String = ''): TFDquery;
      function BuscaSequenciaGenerator(const Generator: String): Integer;
      function  CanDel(const sNomTab, sFK: String; sValue : Integer) : Boolean;
    end;

implementation

uses
  UConexaoControle, SysUtils, Forms, Windows, FireDAC.Stan.param, DB;

{ TControle }

function TControle.BuscaSequenciaGenerator(const Generator: String): Integer;
var
  qry: TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   qry.SQL.Add(Format('SELECT gen_id(%s, 1) as VALOR from RDB$DATABASE', [Generator]));

   qry.Open;

   Result := qry.FieldByName('VALOR').AsInteger;
  finally
   qry.Free;
  end;
end;

function TControle.CanDel(const sNomTab, sFK: String; sValue: Integer): Boolean;
var
  qry, qryDep: TFDQuery;
  sTabDep : String;
begin
  Result := True;
  qry := ConexaoControle.NewQuery;

  try
   // Obtendo todas as tabelas dependentes
   qry.SQL.Clear;
   qry.SQL.Add(' SELECT                                                                                             ');
   qry.SQL.Add('  I1.RDB$RELATION_NAME, S.RDB$FIELD_NAME, R.RDB$DESCRIPTION                                         ');
   qry.SQL.Add(' FROM RDB$INDICES I1 INNER JOIN RDB$INDICES        I2 ON I1.RDB$FOREIGN_KEY   = I2.RDB$INDEX_NAME   ');
   qry.SQL.Add('                     INNER JOIN RDB$INDEX_SEGMENTS S  ON I1.RDB$INDEX_NAME    = S.RDB$INDEX_NAME    ');
   qry.SQL.Add('                     INNER JOIN RDB$RELATIONS      R  ON I1.RDB$RELATION_NAME = R.RDB$RELATION_NAME ');
   qry.SQL.Add(' WHERE I2.RDB$RELATION_NAME = :sNomTab                                                              ');

   qry.ParamByName('sNomTab').AsString := sNomTab;
   qry.Open;

   if qry.IsEmpty then
    Result := True
   else
    begin
     qry.First;
     try
      qryDep := ConexaoControle.NewQuery;

      while not qry.EOF do
       begin
        sTabDep := Trim(qry.FieldByName('RDB$RELATION_NAME').AsString);

        qryDep.SQL.Clear;
        qryDep.SQL.Add(Format(' SELECT CODIGO FROM %s', [sTabDep]));
        qryDep.SQL.Add(Format(' WHERE CODIGO_%s = %d',  [sFK, sValue]));
        qryDep.Open;

        if qryDep.IsEmpty then
         qry.Next
        else
         begin
          Application.MessageBox('N�o � poss�vel excluir o registro pois o mesmo possui depend�ncia.', 'Aten��o', MB_ICONWARNING + MB_OK);
          Result := False;
          Exit;
         end;
       end;
     finally
      FreeAndNil(qryDep);
     end;
    end;
  finally
   FreeAndNil(qry);
  end;
end;

function TControle.CarregarGrid(const NomeTabela, Filter: String;
  Sort: String): TFDquery;
var
  qry: TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   qry.SQL.Add(Format(' SELECT * FROM %s ', [NomeTabela]));

   if not (Trim(Filter) = '') then
     qry.SQL.Add(Format(' WHERE %s ', [Filter]));

   if not (Trim(Sort) = '') then
    qry.SQL.Add(Format(' ORDER BY %s ', [Sort]));

   qry.Open;

   Result := qry;
  finally
  end;
end;

end.
