unit UReceitaControle;

interface

uses
  UControle, FireDAC.Comp.Client, FIREDAC.DApt;

type
  TReceitaControle = class(TControle)
  public
    function CarregarGrid(const NomeTabela, Filter: String; Sort: String = ''): TFDquery;
  end;

var
  ReceitaControle : TReceitaControle;

implementation

uses
  DB, UConexaoControle, SysUtils;

{ TVendasControle }

function TReceitaControle.CarregarGrid(const NomeTabela, Filter: String;
  Sort: String): TFDquery;
var
  qry: TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   qry.SQL.Add(' SELECT DISTINCT V.CODIGO_TECNICO,                                                       ');
   qry.SQL.Add('                 T.NOME AS NOME_TECNICO,                                                 ');
   qry.SQL.Add('                 V.DATA                                                                  ');
   qry.SQL.Add(Format(' FROM %s V INNER JOIN TBLTECNICO T ON V.CODIGO_TECNICO = T.CODIGO ', [NomeTabela]) );

   if not (Trim(Filter) = '') then
     qry.SQL.Add(Format(' WHERE %s ', [Filter]));

   if not (Trim(Sort) = '') then
    qry.SQL.Add(Format(' ORDER BY %s ', [Sort]));

   qry.Open;

   Result := qry;
  finally
  end;
end;

end.
