unit UfrmCadastroItemVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmCadastroPadrao, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, UfrmCadastroVendas,
  Vcl.Mask;

type
  TfrmCadastroItemVenda = class(TfrmCadastroPadrao)
    gpbQuantidade: TGroupBox;
    edtQuantidade: TEdit;
    gpbProduto: TGroupBox;
    dlkpProduto: TDBLookupComboBox;
    TFDQueryProdutos: TFDQuery;
    dsProdutos: TDataSource;
    GroupBox1: TGroupBox;
    dbeValorUnitario: TDBEdit;
    gpbValorTotal: TGroupBox;
    edtValorTotal: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure edtQuantidadeChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure Destruir;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroItemVenda: TfrmCadastroItemVenda;

implementation

uses
  UfrmVendas;

{$R *.dfm}

procedure TfrmCadastroItemVenda.btnGravarClick(Sender: TObject);
begin
  inherited;
  frmCadastroVendas.FDMemItens.FieldByName('CODIGO_PRODUTO').AsInteger := dlkpProduto.KeyValue;
  frmCadastroVendas.FDMemItens.FieldByName('NOME_PRODUTO').AsString    := dlkpProduto.Text;
  frmCadastroVendas.FDMemItens.FieldByName('VALOR').AsFloat            := StrToFloat(dbeValorUnitario.Text);
  frmCadastroVendas.FDMemItens.FieldByName('QUANTIDADE').AsInteger     := StrToInt(edtQuantidade.Text);
  frmCadastroVendas.FDMemItens.FieldByName('TOTAL_ITEM').AsFloat       := StrToFloat(edtValorTotal.Text);
  frmCadastroVendas.FDMemItens.FieldByName('CONTROLE').AsString        := TFDQueryProdutos.FieldByName('CONTROLE').AsString;
  frmCadastroVendas.FDMemItens.FieldByName('NEW').AsString             := 'S';
  frmCadastroVendas.FDMemItens.Post;

  frmCadastroVendas.TFDQueryitens.Close;
  frmCadastroVendas.TFDQueryitens.Data := frmCadastroVendas.FDMemItens;

  frmCadastroVendas.lblVlrTotalPedido.Caption := FormatFloat(',0.00', (StrToFloat(frmCadastroVendas.lblVlrTotalPedido.Caption) + StrToFloat(edtValorTotal.Text)));

  Self.Close;

  frmCadastroVendas.FDMemItens.EnableControls;
end;

procedure TfrmCadastroItemVenda.Destruir;
begin
  frmCadastroItemVenda := nil;
end;

procedure TfrmCadastroItemVenda.edtQuantidadeChange(Sender: TObject);
begin
  inherited;
  edtValorTotal.Text := FloatToStr(StrToFloat(dbeValorUnitario.EditText) * StrToInt(edtQuantidade.Text));
end;

procedure TfrmCadastroItemVenda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Destruir;
end;

procedure TfrmCadastroItemVenda.FormShow(Sender: TObject);
begin
  inherited;
  TFDQueryProdutos.Open;
end;

end.
