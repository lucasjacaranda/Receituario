inherited frmVendas: TfrmVendas
  Caption = 'Pedido de venda'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited DBGrid: TDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO'
        Title.Caption = 'C'#243'digo do Pedido'
        Width = 97
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODIGO_CLIENTE'
        Title.Caption = 'C'#243'digo do Cliente'
        Width = 93
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_CLIENTE'
        Title.Caption = 'Nome do Cliente'
        Width = 436
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA'
        Title.Caption = 'Data do Pedido'
        Width = 115
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STATUS'
        Title.Caption = 'Status do Pedido'
        Width = 130
        Visible = True
      end>
  end
end
