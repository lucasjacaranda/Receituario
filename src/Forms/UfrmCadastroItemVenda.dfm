inherited frmCadastroItemVenda: TfrmCadastroItemVenda
  Caption = 'Cadastro Item da Venda'
  ClientHeight = 110
  ClientWidth = 686
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 692
  ExplicitHeight = 139
  PixelsPerInch = 96
  TextHeight = 13
  inherited Bevel1: TBevel
    Width = 686
    Height = 67
    ExplicitWidth = 686
    ExplicitHeight = 67
  end
  inherited pnlBotoes: TPanel
    Top = 67
    Width = 686
    TabOrder = 3
    ExplicitTop = 67
    ExplicitWidth = 686
    inherited btnGravar: TSpeedButton
      Left = 285
      OnClick = btnGravarClick
      ExplicitLeft = 285
    end
    inherited btnCancelar: TSpeedButton
      Left = 350
      ExplicitLeft = 350
    end
  end
  object gpbQuantidade: TGroupBox
    Left = 472
    Top = 8
    Width = 89
    Height = 49
    Caption = ' Quantidade'
    TabOrder = 2
    object edtQuantidade: TEdit
      Left = 10
      Top = 18
      Width = 70
      Height = 21
      MaxLength = 60
      NumbersOnly = True
      TabOrder = 0
      OnChange = edtQuantidadeChange
    end
  end
  object gpbProduto: TGroupBox
    Left = 8
    Top = 8
    Width = 353
    Height = 49
    Caption = ' Produto'
    TabOrder = 0
    object dlkpProduto: TDBLookupComboBox
      Left = 10
      Top = 18
      Width = 327
      Height = 21
      DataField = 'CODIGO_PRODUTO'
      DataSource = frmCadastroVendas.dsItens
      KeyField = 'CODIGO'
      ListField = 'NOME'
      ListSource = dsProdutos
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 372
    Top = 8
    Width = 89
    Height = 49
    Caption = ' Valor Uni'#225'tio'
    TabOrder = 1
    object dbeValorUnitario: TDBEdit
      Left = 10
      Top = 18
      Width = 70
      Height = 21
      DataField = 'VALOR'
      DataSource = dsProdutos
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
  end
  object gpbValorTotal: TGroupBox
    Left = 567
    Top = 8
    Width = 111
    Height = 49
    Caption = ' Valor Total'
    TabOrder = 4
    object edtValorTotal: TEdit
      Left = 10
      Top = 18
      Width = 87
      Height = 21
      Enabled = False
      MaxLength = 60
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 0
    end
  end
  object TFDQueryProdutos: TFDQuery
    Connection = dmConexao.FDReceituario
    SQL.Strings = (
      'SELECT CODIGO, NOME, VALOR, CONTROLE FROM TBLPRODUTOS')
    Left = 208
    Top = 16
  end
  object dsProdutos: TDataSource
    DataSet = TFDQueryProdutos
    Left = 288
    Top = 16
  end
end
