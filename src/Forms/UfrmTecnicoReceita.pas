unit UfrmTecnicoReceita;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmGridPadrao, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfrmTecnicoReceita = class(TfrmGridPadrao)
    procedure FormCreate(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure CarregarGrid;
    procedure AbrirCadastro;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTecnicoReceita: TfrmTecnicoReceita;

implementation

uses
  UfrmSolicitacaoReceita, UReceitaControle;

{$R *.dfm}

{ TfrmTecnicoReceita }

procedure TfrmTecnicoReceita.AbrirCadastro;
begin
  if TForm(frmSolicitacaoReceita) = nil then
   Application.CreateForm(TfrmSolicitacaoReceita, frmSolicitacaoReceita);

  frmSolicitacaoReceita.BringToFront;
  frmSolicitacaoReceita.Show;
end;

procedure TfrmTecnicoReceita.btnEditarClick(Sender: TObject);
begin
  if dsGrid.DataSet.RecordCount > 0 then
   AbrirCadastro
  else
   begin
    Application.MessageBox('N�o existem receitas aguardando assinatura.', 'Aten��o', MB_ICONWARNING + MB_OK);
    Abort;
   end;
end;

procedure TfrmTecnicoReceita.CarregarGrid;
begin
  dsGrid.DataSet := ReceitaControle.CarregarGrid('TBLVENDA', 'V.STATUS = ''A''', 'CODIGO_TECNICO');
end;

procedure TfrmTecnicoReceita.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  frmTecnicoReceita := nil;
  inherited;
end;

procedure TfrmTecnicoReceita.FormCreate(Sender: TObject);
begin
  CarregarGrid;
end;

end.
