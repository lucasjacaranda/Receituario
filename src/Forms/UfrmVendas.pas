unit UfrmVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmGridPadrao, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfrmVendas = class(TfrmGridPadrao)
    procedure FormCreate(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure CarregarGrid;
    procedure ExcluirRegistro;
    procedure AbrirCadastro;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVendas: TfrmVendas;

implementation

uses
  UVendasControle, UfrmCadastroVendas;

{$R *.dfm}

{ TfrmVendas }

procedure TfrmVendas.AbrirCadastro;
begin
  if TForm(frmCadastroVendas) = nil then
   Application.CreateForm(TfrmCadastroVendas, frmCadastroVendas);

  frmCadastroVendas.BringToFront;
  frmCadastroVendas.Show;
end;

procedure TfrmVendas.btnEditarClick(Sender: TObject);
begin
  inherited;
  AbrirCadastro;
end;

procedure TfrmVendas.btnExcluirClick(Sender: TObject);
begin
  inherited;
  ExcluirRegistro;
end;

procedure TfrmVendas.btnIncluirClick(Sender: TObject);
begin
  inherited;
  AbrirCadastro;
end;

procedure TfrmVendas.CarregarGrid;
begin
  dsGrid.DataSet := VendasControle.CarregarGrid('TBLVENDA', '', 'CODIGO');
end;

procedure TfrmVendas.ExcluirRegistro;
begin
 if VendasControle.CanDel('TBLVENDA', 'VENDA', dsGrid.DataSet.FieldByName('CODIGO').AsInteger) then
  dsGrid.DataSet.Delete;
end;

procedure TfrmVendas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmVendas := nil;
  inherited;
end;

procedure TfrmVendas.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarGrid;
end;

end.
