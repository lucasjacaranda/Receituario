unit UfrmSolicitacaoReceita;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmCadastroPadrao, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, UfrmTecnicoReceita,
  Data.DB, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, UdmConexao;

type
  TfrmSolicitacaoReceita = class(TfrmCadastroPadrao)
    gpbTecnico: TGroupBox;
    dedtCodigoTecnico: TDBEdit;
    dedtNomeTecnico: TDBEdit;
    gpbVendas: TGroupBox;
    gpbItensVenda: TGroupBox;
    dbgVendas: TDBGrid;
    dbgItens: TDBGrid;
    dsVendas: TDataSource;
    dsItens: TDataSource;
    TFDQueryVendas: TFDQuery;
    TFDQueryItens: TFDQuery;
    TFDQueryItensNOME_PRODUTO: TStringField;
    TFDQueryItensVALOR_PRODUTO: TFMTBCDField;
    TFDQueryItensQUANTIDADE: TIntegerField;
    TFDQueryItensTOTAL_ITEM: TFMTBCDField;
    TFDQueryItensPORCENTAGEM: TStringField;
    TFDQueryItensCODIGO_VENDA: TIntegerField;
    TFDQueryVendasCODIGO: TIntegerField;
    TFDQueryVendasNOME_CLIENTE: TStringField;
    TFDQueryVendasDATA: TDateField;
    TFDQueryVendasSTATUS_RECEITA: TStringField;
    TFDQueryVendasSTATUS: TStringField;
    TFDQueryVendasTOTAL_VENDA: TFMTBCDField;
    TFDQueryVendasASSINAR: TStringField;
    procedure btnGravarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgVendasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure TFDQueryVendasAfterScroll(DataSet: TDataSet);
    procedure dbgVendasCellClick(Column: TColumn);
  private
    procedure BuscarInformacoes;
    procedure FiltrarItens;
    procedure AssinarReceita;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSolicitacaoReceita: TfrmSolicitacaoReceita;

implementation

uses
  UITypes, Types;

{$R *.dfm}

procedure TfrmSolicitacaoReceita.BuscarInformacoes;
begin
  TFDQueryVendas.ParamByName('iCodigoTecnico').AsInteger := StrToInt(dedtCodigoTecnico.EditText);
  TFDQueryVendas.Open;

  if TFDQueryVendas.RecordCount > 0 then
   TFDQueryItens.Open;
end;

procedure TfrmSolicitacaoReceita.AssinarReceita;
begin
  TFDQueryVendas.DisableControls;
  TFDQueryItens.DisableControls;

  TFDQueryVendas.Edit;
  TFDQueryVendasSTATUS.AsString := 'C';
  TFDQueryVendas.Post;

  TFDQueryVendas.Close;
  TFDQueryItens.Close;
  BuscarInformacoes;

  TFDQueryVendas.EnableControls;
  TFDQueryItens.EnableControls;

  dbgVendas.SelectedIndex := 1;
end;

procedure TfrmSolicitacaoReceita.btnGravarClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmSolicitacaoReceita.dbgVendasCellClick(Column: TColumn);
begin
 if dbgVendas.SelectedField.FieldName = 'ASSINAR' then
  begin
   AssinarReceita;
  end;
end;

procedure TfrmSolicitacaoReceita.dbgVendasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  BUTTON: Integer;
  R: TRect;
  bcolor: TColor;
begin
  if TFDQueryVendas.RecordCount > 0 then
   begin
    if Column.FieldName = 'ASSINAR' then
     begin
      dbgVendas.Canvas.FillRect(Rect);
      BUTTON := 0;

      R := Rect;

      DrawFrameControl(dbgVendas.Canvas.Handle,R,BUTTON, BUTTON or BUTTON);

      bcolor                       := dbgVendas.Canvas.Brush.Color; // Guarda a cor de fundo original
      dbgVendas.Canvas.Brush.Color := clBtnFace; // Muda a cor de fundo

      DrawText(dbgVendas.Canvas.Handle, 'Assinar Receita', 7, R, DT_VCENTER or DT_CENTER);

      dbgVendas.Canvas.Brush.Color := bcolor; // Devolve a cor original
     end;
   end;
end;

procedure TfrmSolicitacaoReceita.FiltrarItens;
begin
  TFDQueryItens.Filtered := False;
  TFDQueryItens.Filter   := 'CODIGO_VENDA = ' + TFDQueryVendas.FieldByname('CODIGO').AsString;
  TFDQueryItens.Filtered := True;
end;

procedure TfrmSolicitacaoReceita.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmSolicitacaoReceita := nil;
end;

procedure TfrmSolicitacaoReceita.FormShow(Sender: TObject);
begin
  inherited;
  BuscarInformacoes;
end;

procedure TfrmSolicitacaoReceita.TFDQueryVendasAfterScroll(DataSet: TDataSet);
begin
  inherited;
  FiltrarItens;
end;

end.
