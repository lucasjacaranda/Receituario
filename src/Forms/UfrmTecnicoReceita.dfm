inherited frmTecnicoReceita: TfrmTecnicoReceita
  Caption = 'T'#233'cnico Agr'#237'cola Respons'#225'vel pela Receita'
  ClientHeight = 367
  ClientWidth = 720
  OnCreate = FormCreate
  ExplicitWidth = 726
  ExplicitHeight = 396
  PixelsPerInch = 96
  TextHeight = 13
  inherited TBevel: TBevel
    Width = 720
    Height = 324
    ExplicitWidth = 720
    ExplicitHeight = 324
  end
  inherited pnlBotoes: TPanel
    Top = 324
    Width = 720
    ExplicitTop = 324
    ExplicitWidth = 720
    inherited btnExcluir: TSpeedButton
      Left = 56
      Top = 6
      Enabled = False
      Visible = False
      ExplicitLeft = 56
      ExplicitTop = 6
    end
    inherited btnIncluir: TSpeedButton
      Left = 188
      Top = 6
      Enabled = False
      Visible = False
      ExplicitLeft = 188
      ExplicitTop = 6
    end
    inherited btnEditar: TSpeedButton
      Left = 0
      Top = 6
      ExplicitLeft = 0
      ExplicitTop = 6
    end
    inherited btnFechar: TSpeedButton
      Left = 666
      ExplicitLeft = 666
    end
  end
  inherited DBGrid: TDBGrid
    Width = 720
    Height = 324
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO_TECNICO'
        Title.Caption = 'C'#243'digo do T'#233'cnico'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_TECNICO'
        Title.Caption = 'Nome do T'#233'cnico'
        Width = 576
        Visible = True
      end>
  end
end
