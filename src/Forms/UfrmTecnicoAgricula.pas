unit UfrmTecnicoAgricula;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmCadastroPadrao, Vcl.Buttons,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.StdCtrls, UfrmVendas;

type
  TfrmTecnicoAgricula = class(TfrmCadastroPadrao)
    dlkpTecnico: TDBLookupComboBox;
    TFDQueryTecnicos: TFDQuery;
    dsTecnicos: TDataSource;
    gpbTecnico: TGroupBox;
    procedure FormShow(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTecnicoAgricula: TfrmTecnicoAgricula;

implementation

uses
  UfrmCadastroVendas;

{$R *.dfm}

procedure TfrmTecnicoAgricula.btnCancelarClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TfrmTecnicoAgricula.btnGravarClick(Sender: TObject);
begin
  inherited;
  frmCadastroVendas.CodigoTecnico := dlkpTecnico.KeyValue;
  ModalResult := mrOk;
end;

procedure TfrmTecnicoAgricula.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TFDQueryTecnicos.Close;
  frmTecnicoAgricula := nil;
end;

procedure TfrmTecnicoAgricula.FormShow(Sender: TObject);
begin
  inherited;
  TFDQueryTecnicos.Open;
end;

end.
