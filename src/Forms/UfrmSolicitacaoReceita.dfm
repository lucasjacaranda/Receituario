inherited frmSolicitacaoReceita: TfrmSolicitacaoReceita
  Caption = 'Solicita'#231#227'o de Receita'
  ClientHeight = 476
  ClientWidth = 904
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 910
  ExplicitHeight = 505
  PixelsPerInch = 96
  TextHeight = 13
  inherited Bevel1: TBevel
    Width = 904
    Height = 433
    ExplicitWidth = 904
    ExplicitHeight = 433
  end
  inherited pnlBotoes: TPanel
    Top = 433
    Width = 904
    ExplicitTop = 433
    ExplicitWidth = 904
    inherited btnGravar: TSpeedButton
      Left = 431
      OnClick = btnGravarClick
      ExplicitLeft = 353
    end
    inherited btnCancelar: TSpeedButton
      Left = 478
      ExplicitLeft = 478
    end
  end
  object gpbTecnico: TGroupBox
    Left = 10
    Top = 8
    Width = 871
    Height = 44
    Caption = ' T'#233'cnico Agr'#237'cola Respons'#225'vel'
    TabOrder = 1
    object dedtCodigoTecnico: TDBEdit
      Left = 10
      Top = 16
      Width = 87
      Height = 21
      DataField = 'CODIGO_TECNICO'
      DataSource = frmTecnicoReceita.dsGrid
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object dedtNomeTecnico: TDBEdit
      Left = 112
      Top = 16
      Width = 745
      Height = 21
      DataField = 'NOME_TECNICO'
      DataSource = frmTecnicoReceita.dsGrid
      Enabled = False
      ReadOnly = True
      TabOrder = 1
    end
  end
  object gpbVendas: TGroupBox
    Left = 10
    Top = 64
    Width = 871
    Height = 161
    Caption = ' Vendas'
    TabOrder = 2
    object dbgVendas: TDBGrid
      Left = 10
      Top = 16
      Width = 847
      Height = 137
      DataSource = dsVendas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = dbgVendasCellClick
      OnDrawColumnCell = dbgVendasDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'CODIGO'
          Title.Caption = 'C'#243'digo da Venda'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_CLIENTE'
          Title.Caption = 'Cliente'
          Width = 307
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATA'
          Title.Caption = 'Data da Venda'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL_VENDA'
          Title.Caption = 'Tota da venda'
          Width = 87
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS_VENDA'
          Title.Caption = 'Status da Venda'
          Width = 131
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ASSINAR'
          Title.Caption = 'Assinatura'
          Width = 131
          Visible = True
        end>
    end
  end
  object gpbItensVenda: TGroupBox
    Left = 10
    Top = 240
    Width = 871
    Height = 185
    Caption = ' Produtos com controle Especial'
    TabOrder = 3
    object dbgItens: TDBGrid
      Left = 3
      Top = 21
      Width = 847
      Height = 161
      DataSource = dsItens
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOME_PRODUTO'
          Title.Caption = 'Produto'
          Width = 459
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADE'
          Title.Caption = 'Quantidade'
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_PRODUTO'
          Title.Caption = 'Valor Unit'#225'rio'
          Width = 107
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL_ITEM'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PORCENTAGEM'
          Title.Caption = 'Percentual'
          Visible = True
        end>
    end
  end
  object dsVendas: TDataSource
    DataSet = TFDQueryVendas
    Left = 186
    Top = 160
  end
  object dsItens: TDataSource
    DataSet = TFDQueryItens
    Left = 186
    Top = 352
  end
  object TFDQueryVendas: TFDQuery
    AfterScroll = TFDQueryVendasAfterScroll
    Connection = dmConexao.FDReceituario
    SQL.Strings = (
      'SELECT V.CODIGO,'
      '       C.NOME AS NOME_CLIENTE,'
      '       V.DATA,'
      '       CASE V.STATUS WHEN '#39'A'#39' THEN '#39'Aguardando Receita'#39
      '       END AS STATUS_VENDA,'
      '       V.STATUS,'
      '       V.TOTAL_VENDA'
      
        'FROM TBLVENDA V INNER JOIN TBLCLIENTE C ON V.CODIGO_CLIENTE = C.' +
        'CODIGO'
      'WHERE V.CODIGO_TECNICO = :iCodigoTecnico'
      '  AND V.STATUS = '#39'A'#39
      'ORDER BY V.CODIGO')
    Left = 186
    Top = 112
    ParamData = <
      item
        Name = 'ICODIGOTECNICO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object TFDQueryVendasCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TFDQueryVendasNOME_CLIENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_CLIENTE'
      Origin = 'NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 60
    end
    object TFDQueryVendasDATA: TDateField
      FieldName = 'DATA'
      Origin = '"DATA"'
      Required = True
    end
    object TFDQueryVendasSTATUS_RECEITA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'STATUS_VENDA'
      Origin = 'STATUS_VENDA'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 18
    end
    object TFDQueryVendasSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object TFDQueryVendasTOTAL_VENDA: TFMTBCDField
      FieldName = 'TOTAL_VENDA'
      Origin = 'TOTAL_VENDA'
      Precision = 18
      Size = 1
    end
    object TFDQueryVendasASSINAR: TStringField
      FieldKind = fkCalculated
      FieldName = 'ASSINAR'
      Size = 1
      Calculated = True
    end
  end
  object TFDQueryItens: TFDQuery
    Connection = dmConexao.FDReceituario
    SQL.Strings = (
      'SELECT P.NOME AS NOME_PRODUTO,'
      '       P.VALOR AS VALOR_PRODUTO,'
      '       I.QUANTIDADE,'
      '       I.TOTAL_ITEM,'
      
        '       CAST(((100 * I.TOTAL_ITEM) / V.TOTAL_VENDA) AS NUMERIC(3,' +
        '2)) || '#39'%'#39' AS PORCENTAGEM,'
      '       I.CODIGO_VENDA'
      
        'FROM TBLITENSVENDA I INNER JOIN TBLPRODUTOS P ON I.CODIGO_PRODUT' +
        'O = P.CODIGO'
      
        '                     INNER JOIN TBLVENDA V    ON I.CODIGO_VENDA ' +
        '  = V.CODIGO  '
      'WHERE P.CONTROLE = '#39'S'#39)
    Left = 186
    Top = 296
    object TFDQueryItensNOME_PRODUTO: TStringField
      FieldName = 'NOME_PRODUTO'
      Origin = 'NOME_PRODUTO'
      Size = 60
    end
    object TFDQueryItensVALOR_PRODUTO: TFMTBCDField
      FieldName = 'VALOR_PRODUTO'
      Origin = 'VALOR_PRODUTO'
      Precision = 18
      Size = 2
    end
    object TFDQueryItensQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
    end
    object TFDQueryItensTOTAL_ITEM: TFMTBCDField
      FieldName = 'TOTAL_ITEM'
      Origin = 'TOTAL_ITEM'
      Precision = 18
      Size = 2
    end
    object TFDQueryItensPORCENTAGEM: TStringField
      FieldName = 'PORCENTAGEM'
      Origin = 'PORCENTAGEM'
      Size = 8
    end
    object TFDQueryItensCODIGO_VENDA: TIntegerField
      FieldName = 'CODIGO_VENDA'
      Origin = 'CODIGO_VENDA'
      Required = True
    end
  end
end
