unit UfrmCadastroVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UfrmCadastroPadrao, Vcl.Buttons,
  Vcl.ExtCtrls, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.StdCtrls, UdmConexao,
  Vcl.DBCtrls;

type
  TfrmCadastroVendas = class(TfrmCadastroPadrao)
    gpbNomeCliente: TGroupBox;
    gpbDataPedido: TGroupBox;
    dtpDataVenda: TDateTimePicker;
    pnlGrid: TPanel;
    dbgItemsVenda: TDBGrid;
    btnAdicionaItem: TSpeedButton;
    btnExcluirItem: TSpeedButton;
    dsItens: TDataSource;
    TFDQueryitens: TFDQuery;
    dlkpCliente: TDBLookupComboBox;
    dsClientes: TDataSource;
    TFDQueryClientes: TFDQuery;
    FDMemItens: TFDMemTable;
    FDMemItensCODIGO_PRODUTO: TIntegerField;
    FDMemItensQUANTIDADE: TIntegerField;
    FDMemItensNOME_PRODUTO: TStringField;
    FDMemItensVALOR: TFMTBCDField;
    FDMemItensTOTAL_ITEM: TFMTBCDField;
    lblStrTotalPedido: TLabel;
    lblVlrTotalPedido: TLabel;
    FDMemItensNEW: TStringField;
    FDMemItensCONTROLE: TStringField;
    procedure FormShow(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAdicionaItemClick(Sender: TObject);
    procedure btnExcluirItemClick(Sender: TObject);
  private
    FCodigoTecnico : Integer;
    procedure LoadObject;
    procedure SaveObject;
    procedure SaveObjectList;
    procedure Destruir;
    procedure AbrirCadastro;
    procedure SalvarCadastro;
    procedure CarregarGrid;
    procedure AdicionarItem;
    procedure ExcluirRegistro;
    { Private declarations }
  public
    property CodigoTecnico : Integer read FCodigoTecnico write FCodigoTecnico;
    { Public declarations }
  end;

var
  frmCadastroVendas: TfrmCadastroVendas;

implementation

uses
  UVendasControle, UVendasDAO, UVendas, UfrmVendas, UConexaoControle,
  UfrmTecnicoAgricula, UfrmCadastroItemVenda;

var
  Venda : TVendas;

{$R *.dfm}

{ TfrmCadastroVendas }

procedure TfrmCadastroVendas.AbrirCadastro;
begin
  if not Assigned(Venda) then
   Venda := TVendas.Create;

  TFDQueryClientes.Open;

  if frmVendas.dsGrid.DataSet.Tag = 5 then
   begin
    TFDQueryitens.ParamByName('iCodigoVenda').AsInteger := frmVendas.dsGrid.DataSet.FieldByName('CODIGO').AsInteger;

    LoadObject;
   end
  else
   begin
    TFDQueryitens.ParamByName('iCodigoVenda').AsInteger := 0;

    dtpDataVenda.Date          := Now;
    lblVlrTotalPedido.Caption  := '00,00';
   end;

  TFDQueryitens.Open;
  FDMemItens.Data := TFDQueryitens;
end;

procedure TfrmCadastroVendas.AdicionarItem;
begin
  FDMemItens.Append;
  if TForm(frmCadastroItemVenda) = nil then
   Application.CreateForm(TfrmCadastroItemVenda, frmCadastroItemVenda);

  frmCadastroItemVenda.BringToFront;
  frmCadastroItemVenda.Show;
end;

procedure TfrmCadastroVendas.btnAdicionaItemClick(Sender: TObject);
begin
  inherited;
  AdicionarItem;
end;

procedure TfrmCadastroVendas.btnCancelarClick(Sender: TObject);
begin
  inherited;
  Destruir;
end;

procedure TfrmCadastroVendas.btnExcluirItemClick(Sender: TObject);
begin
  ExcluirRegistro;
end;

procedure TfrmCadastroVendas.btnGravarClick(Sender: TObject);
begin
  inherited;
  SalvarCadastro;
end;

procedure TfrmCadastroVendas.Destruir;
begin
  FreeAndNil(Venda);
  frmCadastroVendas := nil;
end;

procedure TfrmCadastroVendas.ExcluirRegistro;
begin
 if FDMemItens.RecordCount = 0 then
  begin
   Application.MessageBox('N�o h� registros para serem exclu�dos!', 'Erro', MB_ICONERROR + MB_OK);
   Abort;
  end;

 if not Application.MessageBox('Deseja realmente excluir o registro selecionado?', 'Pergunta', MB_YESNO) = ID_YES then
  Abort;

 frmCadastroVendas.lblVlrTotalPedido.Caption := FormatFloat(',0.00', (StrToFloat(frmCadastroVendas.lblVlrTotalPedido.Caption) - FDMemItens.FieldByName('TOTAL_ITEM').AsFloat));

 if FDMemItens.FieldByName('NEW').AsString = 'S' then
  FDMemItens.Delete
 else
  begin
   TFDQueryitens.RecNo := FDMemItens.RecNo;
   TFDQueryitens.Delete;
   FDMemItens.Data := TFDQueryitens;
  end;
end;

procedure TfrmCadastroVendas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CarregarGrid;
  TFDQueryClientes.Close;
  Destruir;
end;

procedure TfrmCadastroVendas.FormShow(Sender: TObject);
begin
  inherited;
  AbrirCadastro;
end;

procedure TfrmCadastroVendas.LoadObject;
begin
  VendasDAO.Carregar(Venda, frmVendas.dsGrid.DataSet.FieldByName('CODIGO').AsInteger);
  TFDQueryClientes.Locate('CODIGO', Venda.Codigo);
  dtpDataVenda.Date         := Venda.Data;
  lblVlrTotalPedido.Caption := FloatToStr(Venda.TotalVenda);
end;

procedure TfrmCadastroVendas.CarregarGrid;
begin
  frmVendas.dsGrid.DataSet := VendasControle.CarregarGrid('TBLVENDA', '', 'CODIGO');
  frmVendas.dsGrid.DataSet.EnableControls;
end;

procedure TfrmCadastroVendas.SalvarCadastro;
begin
  if FDMemItens.RecordCount > 0 then
   begin
    if frmVendas.dsGrid.DataSet.Tag = 2 then
     Venda.Codigo := VendasControle.BuscaSequenciaGenerator('GNR_VENDA');

    Venda.CodigoCliente := dlkpCliente.KeyValue;
    Venda.Data          := dtpDataVenda.Date;
    Venda.TotalVenda    := StrToFloat(lblVlrTotalPedido.Caption);

    if VendasControle.ChecarProdutosEspeciais(TFDQueryitens) then
     begin
      if TForm(frmTecnicoAgricula) = nil then
       Application.CreateForm(TfrmTecnicoAgricula, frmTecnicoAgricula);

      TForm(frmTecnicoAgricula).BringToFront;

      if TForm(frmTecnicoAgricula).ShowModal = mrOK then
       begin
        Venda.CodigoTecnico := CodigoTecnico;
        Venda.Status        := 'A';
       end
      else
       Abort;
     end
    else
      Venda.Status := 'C';

    SaveObject;

    Self.Close;
   end
  else
   begin
    Application.MessageBox('Para realizar o pedido de venda � necess�rio lan�ar pelo menos um Produto!', 'Aten��o', MB_ICONWARNING + MB_OK);
    Abort;
   end;
end;

procedure TfrmCadastroVendas.SaveObject;
var
  qry : TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   if frmVendas.dsGrid.DataSet.Tag = 5 then
    begin
     qry.SQL.Add(' UPDATE TBLVENDA               ');
     qry.SQL.Add(' SET CODIGO_CLIENTE = :iCodigoCliente,      ');

     if Venda.CodigoTecnico > 0 then
      qry.SQL.Add('     CODIGO_TECNICO = :iCodigoTecnico,     ');

     qry.SQL.Add('     DATA           = :dData,               ');
     qry.SQL.Add('     STATUS         = :sStatus,             ');
     qry.SQL.Add('     TOTAL_VENDA    = :fTotalVenda          ');
     qry.SQL.Add(' WHERE CODIGO = :iCodigo                    ');
    end
   else
    begin
     qry.SQL.Add(' INSERT INTO TBLVENDA (CODIGO, CODIGO_CLIENTE, CODIGO_TECNICO, DATA, STATUS, TOTAL_VENDA)          ');
     qry.SQL.Add('               VALUES (:iCodigo, :iCodigoCliente, :iCodigoTecnico, :dData, :sStatus, :fTotalVenda) ');
    end;

   qry.ParamByName('iCodigo').AsInteger        := Venda.Codigo;
   qry.ParamByName('iCodigoCliente').AsInteger := Venda.CodigoCliente;

   if Venda.CodigoTecnico > 0 then
    qry.ParamByName('iCodigoTecnico').AsInteger := Venda.CodigoTecnico;

   qry.ParamByName('dData').AsDate             := Venda.Data;
   qry.ParamByName('sStatus').AsString         := Venda.Status;
   qry.ParamByName('fTotalVenda').AsFloat      := Venda.TotalVenda;
   qry.ExecSQL;

   SaveObjectList;
  finally
   FreeAndNil(qry);
  end;
end;

procedure TfrmCadastroVendas.SaveObjectList;
var
  qry : TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   FDMemItens.Filter   := 'NEW = ''S'' ';
   FDMemItens.Filtered := True;
   FDMemItens.First;

   while not FDMemItens.Eof do
    begin
     qry.SQL.Clear;
     qry.SQL.Add(' INSERT INTO TBLITENSVENDA (CODIGO, CODIGO_VENDA, CODIGO_PRODUTO, QUANTIDADE, TOTAL_ITEM)        ');
     qry.SQL.Add('                    VALUES (:iCodigo, :iCodigoVenda, :iCodigoProduto, :iQuantidade, :fTotalItem) ');
     qry.ParamByName('iCodigo').AsInteger        := VendasControle.BuscaSequenciaGenerator('GNR_ITEMVENDA');
     qry.ParamByName('iCodigoVenda').AsInteger   := Venda.Codigo;
     qry.ParamByName('iCodigoProduto').AsInteger := FDMemItens.FieldByName('CODIGO_PRODUTO').AsInteger;
     qry.ParamByName('iQuantidade').AsInteger    := FDMemItens.FieldByName('QUANTIDADE').AsInteger;
     qry.ParamByName('fTotalItem').AsFloat       := FDMemItens.FieldByName('TOTAL_ITEM').AsFloat;
     qry.ExecSQL;

     FDMemItens.Next;
    end;

   FDMemItens.Filtered := False;
  finally
   FreeAndNil(qry);
  end;
end;

end.
