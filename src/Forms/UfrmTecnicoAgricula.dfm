inherited frmTecnicoAgricula: TfrmTecnicoAgricula
  Caption = 'Selecione o T'#233'cnico Agr'#237'cola'
  ClientHeight = 116
  ClientWidth = 421
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 427
  ExplicitHeight = 145
  PixelsPerInch = 96
  TextHeight = 13
  inherited Bevel1: TBevel
    Width = 421
    Height = 73
    ExplicitWidth = 421
    ExplicitHeight = 73
  end
  inherited pnlBotoes: TPanel
    Top = 73
    Width = 421
    ExplicitTop = 73
    ExplicitWidth = 421
    inherited btnGravar: TSpeedButton
      Left = 175
      OnClick = btnGravarClick
      ExplicitLeft = 175
    end
    inherited btnCancelar: TSpeedButton
      Left = 213
      Width = 41
      ExplicitLeft = 213
      ExplicitWidth = 41
    end
  end
  object gpbTecnico: TGroupBox
    Left = 8
    Top = 8
    Width = 401
    Height = 49
    Caption = ' T'#233'cnico Agr'#237'cola'
    TabOrder = 1
    object dlkpTecnico: TDBLookupComboBox
      Left = 10
      Top = 18
      Width = 375
      Height = 21
      DataField = 'CODIGO_TECNICO'
      DataSource = frmVendas.dsGrid
      KeyField = 'CODIGO'
      ListField = 'NOME'
      ListSource = dsTecnicos
      TabOrder = 0
    end
  end
  object TFDQueryTecnicos: TFDQuery
    Connection = dmConexao.FDReceituario
    SQL.Strings = (
      'SELECT CODIGO, NOME FROM TBLTECNICO')
    Left = 208
    Top = 16
  end
  object dsTecnicos: TDataSource
    DataSet = TFDQueryTecnicos
    Left = 288
    Top = 16
  end
end
