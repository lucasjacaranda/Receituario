unit UVendasDAO;

interface

uses
  UVendas;

type
  TVendasDAO = class
  public
    procedure Carregar(Venda : TVendas; iCodigoVenda : Integer);
  end;

var
  VendasDAO : TVendasDAO;

implementation

uses
  SysUtils, FireDAC.Comp.Client, FIREDAC.DApt, FireDAC.Stan.Param, UConexaoControle;

{ TVendasDAO }

procedure TVendasDAO.Carregar(Venda: TVendas; iCodigoVenda: Integer);
var
  qry: TFDQuery;
begin
  qry := ConexaoControle.NewQuery;

  try
   qry.SQL.Add(' SELECT * FROM TBLVENDA       ');
   qry.SQL.Add(' WHERE CODIGO = :iCodigoVenda ');

   qry.ParamByName('iCodigoVenda').AsInteger := iCodigoVenda;
   qry.Open;

   Venda.Codigo        := qry.FieldByName('CODIGO').AsInteger;
   Venda.CodigoCliente := qry.FieldByName('CODIGO_CLIENTE').AsInteger;
   Venda.CodigoTecnico := qry.FieldByName('CODIGO_TECNICO').AsInteger;
   Venda.Data          := qry.FieldByName('DATA').AsDateTime;
   Venda.Status        := qry.FieldByName('STATUS').AsString;
   Venda.TotalVenda    := qry.FieldByName('TOTAL_VENDA').AsFloat;
  finally
   FreeAndNil(qry);
  end;
end;

end.
