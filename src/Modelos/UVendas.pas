unit UVendas;

interface

type
  TVendas = class
  private
    FCodigo,
    FCodigoCliente,
    FCodigoTecnico : Integer;
    FData : TDate;
    FStatus : String;
    FTotalVenda : Double;
  public
    procedure Find(const iCodigoVenda : Integer);

    property Codigo:        Integer read FCodigo         write FCodigo;
    property CodigoCliente: Integer read FCodigoCliente  write FCodigoCliente;
    property CodigoTecnico: Integer read FCodigoTecnico  write FCodigoTecnico;
    property Data:          TDate   read FData           write FData;
    property Status:        String  read FStatus         write FStatus;
    property TotalVenda:    Double  read FTotalVenda     write FTotalVenda;
  end;

implementation

{ TVendas }

uses
  UVendasDAO, SysUtils;

procedure TVendas.Find(const iCodigoVenda: Integer);
var
  VendaDAO: TVendasDAO;
begin
  VendaDAO := TVendasDAO.Create;

  try
   VendaDAO.Carregar(Self, iCodigoVenda);
  finally
   FreeAndNil(VendaDAO);
  end;
end;

end.
