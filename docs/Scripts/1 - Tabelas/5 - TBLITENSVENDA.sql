CREATE TABLE TBLITENSVENDA (
    CODIGO          INTEGER NOT NULL,
    CODIGO_VENDA    INTEGER NOT NULL,
    CODIGO_PRODUTO  INTEGER NOT NULL,
    QUANTIDADE      INTEGER,
    TOTAL_ITEM      NUMERIC(12,2)
);

/******************************************************************************/
/*                                Primary keys                                */
/******************************************************************************/

ALTER TABLE TBLITENSVENDA ADD CONSTRAINT PK_TBLITENSVENDA PRIMARY KEY (CODIGO);

/******************************************************************************/
/*                                Foreign keys                                */
/******************************************************************************/

ALTER TABLE TBLITENSVENDA ADD CONSTRAINT FK_ITENSVENDAPRODUTOS FOREIGN KEY (CODIGO_PRODUTO) REFERENCES TBLPRODUTOS (CODIGO);
ALTER TABLE TBLITENSVENDA ADD CONSTRAINT FK_ITENSVENDAVENDA FOREIGN KEY (CODIGO_VENDA) REFERENCES TBLVENDA (CODIGO);

/******************************************************************************/
/*                                Descriptions                                */
/******************************************************************************/

COMMENT ON TABLE TBLITENSVENDA IS
'Tabela dos Itens da Venda';

/******************************************************************************/
/*                            Fields descriptions                             */
/******************************************************************************/

COMMENT ON COLUMN TBLITENSVENDA.CODIGO IS
'Código Sequencial item';

COMMENT ON COLUMN TBLITENSVENDA.CODIGO_VENDA IS
'Código da Venda';

COMMENT ON COLUMN TBLITENSVENDA.CODIGO_PRODUTO IS
'Código do Produto';

COMMENT ON COLUMN TBLITENSVENDA.QUANTIDADE IS
'Quantidade de Produto';

COMMENT ON COLUMN TBLITENSVENDA.TOTAL_ITEM IS
'Total do Produto';